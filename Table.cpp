// Name: Alejandro Monroy Reyes
// Loginid: monroy re
// CSCI 455 PA5
// Fall 2016

// Table.cpp  Table class implementation


/*
 * Modified 11/22/11 by CMB
 *   changed name of constructor formal parameter to match .h file
 */

#include "Table.h"

#include <iostream>
#include <string>
#include <cassert>


// listFuncs.h has the definition of Node and its methods.  -- when
// you complete it it will also have the function prototypes for your
// list functions.  With this #include, you can use Node type (and
// Node*, and ListType), and call those list functions from inside
// your Table methods, below.

#include "listFuncs.h"


//*************************************************************************


Table::Table() {
    hashSize = HASH_SIZE;
    data = new ListType[hashSize];
    for(int i =0; i < hashSize; i++)
    {
        data[i] = NULL;
    }
}


Table::Table(unsigned int hSize) {
    hashSize = hSize;
    data = new ListType[hashSize];
    for(int i =0; i < hashSize; i++)
    {
        data[i] = NULL;
    }
}

// returns the address of the value or NULL if key is not present
int * Table::lookup(const string &key) {
    return listLookup(data[hashCode(key)], key);
}

// remove an element
// false iff element wasn't present
bool Table::remove(const string &key) {
    ListType p = data[hashCode(key)];
    if(p != NULL)
    {
        if(p->key == key)
        {
            listRemoveFirst(data[hashCode(key)]);
            numEnt--;
            return true;
        }
    p= p->next;
    while(p!=NULL)
    {
        if(p->key == key )
        {
            if(p->next != NULL)
            {
                listRemoveMiddle(data[hashCode(key)], key);
                numEnt--;
                return true;
            }
            else
            {
                listRemoveLast(data[hashCode(key)], key);
                numEnt--;
                return true;
            }   
        }
        p = p->next;
        }
    }
    return false;
}


// insert a new pair into the table
// return false iff this key was already present 
//         (and no change made to table)
bool Table::insert(const string &key, int value) {
    
    bool flag = listInsert(data[hashCode(key)], key , value);
    if(flag){numEnt++;}
    return flag;
}

//return number of entries in the table
int Table::numEntries() const {
    return numEnt;     
}

// print out all the entries in the table, one per line.
// Sample output:
//   joe 32
//   sam 273
//   bob 84
void Table::printAll() const {
    for(int i =0; i < hashSize; i++)
    {   
        ListType p = data[i];
        while(p != NULL)
        {
            cout<<p->key<<" , "<<p->value<<endl;
            p = p->next;
        }   
    }
}

// hashStats: for diagnostic purposes only
// prints out info to let us know if we're getting a good distribution
// of values in the hash table.
// Sample output from this function
//   number of buckets: 997
//   number of entries: 10
//   number of non-empty buckets: 9
//   longest chain: 2
void Table::hashStats(ostream &out) const {
    int numEntries = 0;
    int nonEmptyBuckets= 0 ;
    int longestChain = 0;
    for(int i =0; i < hashSize; i++)
    {
        ListType p = data[i];
        if(p !=NULL)
        {
            nonEmptyBuckets++;
            int currentChain = 0;
            while(p != NULL)
            {   
                currentChain++;
                numEntries++;
                p = p->next;    
            }
            if(currentChain>longestChain)
            {
                longestChain = currentChain;
            }
            }
    }
    out<<"Number of Buckets: " <<hashSize<<endl;
    out<<"Number of Entries: " <<numEntries<<endl;
    out<<"Number of Non Empty Buckets: "<<nonEmptyBuckets<<endl;
    out<<"Longest chain: " <<longestChain<<endl;
}
	
