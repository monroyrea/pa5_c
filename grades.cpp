// Name: Alejandro Monroy Reyes
// Loginid: monroyre
// CSCI 455 PA5
// Fall 2016

/*
 * grades.cpp
 * A program to test the Table class.
 * How to run it:
 *      grades [hashSize]
 * 
 * the optional argument hashSize is the size of hash table to use.
 * if it's not given, the program uses default size (Table::HASH_SIZE)
 *
 */

#include "Table.h"
#include <sstream>

// cstdlib needed for call to atoi
#include <cstdlib>

void execCommand(string command, bool &exit, Table * grades);
void help();

int main(int argc, char * argv[]) {

  // gets the hash table size from the command line

  int hashSize = Table::HASH_SIZE;

  Table * grades;  // Table is dynamically allocated below, so we can call
                   // different constructors depending on input from the user.

  if (argc > 1) {
    hashSize = atoi(argv[1]);  // atoi converts c-string to int

    if (hashSize < 1) {
      cout << "Command line argument (hashSize) must be a positive number" 
       << endl;
      return 1;
    }

    grades = new Table(hashSize);

  }
  else {   // no command line args given -- use default table size
    grades = new Table();
  }


  grades->hashStats(cout); 
  
  bool exit = false; //flag to finish the loop
  string commandLine; 

  
  while(!exit)
  { 
    cout<<"cmd> ";
    getline(cin, commandLine);
    execCommand (commandLine,exit,grades);      
  }

  return 0;
}

//Process the input line and determine which command to execute
//using the Table functions
void execCommand(string commandLine, bool &exit, Table * grades)
{
    string command;
    string name;
    string grade;
    stringstream ss (commandLine);
    ss>> command >> name >> grade;  //Process the command arguments.
    
    if(command == "insert"){    //Insert name,student pair into the grades table.
        if(! grades->insert(name,atoi(grade.c_str()))){
            cout<<"NOT INSERTED:The student is already present"<<endl;
        }
    }
    else if(command == "change"){   //Changes the score of a student.
        int *p = grades->lookup(name);
        if(p == NULL){
            cout<<"NOT PRESENT: The student is not in the table"<<endl;
        }
        else{
            *p = atoi(grade.c_str());
        }
    }
    else if(command == "lookup"){   //Lookup a student and score if it is in the table. 
        int *p = grades->lookup(name);
        if(p == NULL){
            cout<<"NOT PRESENT: The student is not in the table"<<endl;
        }
        else{
            cout<<name<< " , "<< *p <<endl;
        }
    }
    else if(command == "remove"){   //Removes a student from the table.
        if(! grades->remove(name)){
            cout<<"NOT REMOVED: The student does not exist"<<endl; 
        }
    }
    else if(command == "print"){    //Prints all students, scores of the grades table.
        grades->printAll(); 
    }
    else if(command == "size"){ //Prints number of students in the grades table.
        cout<<"Number of entries = " << grades -> numEntries()<<endl;
    }
    else if(command == "stats"){    //Prints grades table stats.
        grades -> hashStats(cout);
    }   
    else if(command == "help"){ //Prints help.
        help();
    }
    else if(command == "quit"){ //Finishes the program.
        exit = true;
    }
    else{
        cout<<"ERROR: Invalid Command"<<endl;
        help();
    }
}

//Prints help on how to use the program.
void help()
{
    cout<<"Use grades [size] to run the program / size = integer number"<<endl;
    cout<<"-------COMMANDS--------"<<endl;
    cout<<"insert name score"<<endl;
    cout<<"     Insert this name and score in the grade table. If this name was already present, print a message to that effect, and don't do the insert."<<endl;
    cout<<"change name newscore"<<endl;
    cout<<"     Change the score for name. Print an appropriate message if this name isn't present."<<endl;
    cout<<"lookup name"<<endl;
    cout<<"     Lookup the name, and print out his or her score, or a message indicating that student is not in the table."<<endl;
    cout<<"remove name"<<endl;
    cout<<"     Remove this student. If this student wasn't in the grade table, print a message to that effect."<<endl;
    cout<<"print"<<endl;
    cout<<"     Prints out all names and scores in the table."<<endl;
    cout<<"size"<<endl;
    cout<<"     Prints out the number of entries in the table."<<endl;
    cout<<"stats"<<endl;
    cout<<"     Prints out statistics about the hash table at this point. (Calls hashStats() method)"<<endl;
    cout<<"help"<<endl;
    cout<<"     Prints out a brief command summary."<<endl;
    cout<<"quit"<<endl;
    cout<<"     Exits the program."<<endl;

}
