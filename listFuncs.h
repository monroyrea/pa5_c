// Name: Alejandro Monroy Reyes
// Loginid: monroyre
// CSCI 455 PA5
// Fall 2016


//*************************************************************************
// Node class definition 
// and declarations for functions on ListType

// Note: we don't need Node in Table.h
// because it's used by the Table class; not by any Table client code.


#ifndef LIST_FUNCS_H
#define LIST_FUNCS_H
  
using namespace std;

struct Node {
  string key;
  int value;

  Node *next;

  Node(const string &theKey, int theValue);

  Node(const string &theKey, int theValue, Node *n);
};


typedef Node * ListType;

//*************************************************************************
//add function headers (aka, function prototypes) for your functions
//that operate on a list here (i.e., each includes a parameter of type
//ListType or ListType&).  No function definitions go in this file.

//Removes firs element of the list
void listRemoveFirst(ListType & list);

//Removes a middle element of the list.
void listRemoveMiddle(ListType & list, string target);

//Remove Las element of the list.
void listRemoveLast(ListType & list, string target);

//Insert a new element to the list if it is not present. 
//return true if an element was removed.
bool listInsert(ListType & list, string target, int value);

//Look for a key in the table
//return NULL if the element was not present. 
int * listLookup(ListType & list, string target);

// keep the following line at the end of the file
#endif
