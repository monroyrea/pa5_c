// Name: Alejandro Monroy Reyes
// Loginid: monroyre
// CSCI 455 PA5
// Fall 2016


#include <iostream>

#include <cassert>

#include "listFuncs.h"

using namespace std;

Node::Node(const string &theKey, int theValue) {
  key = theKey;
  value = theValue;
  next = NULL;
}

Node::Node(const string &theKey, int theValue, Node *n) {
  key = theKey;
  value = theValue;
  next = n;
}

//*************************************************************************
// put the function definitions for your list functions below

//Remove first element of list.
void listRemoveFirst(ListType & list)
{
    ListType cleanList = list;
    list = list->next;
    delete cleanList;
}

//Remove middle element of list. 
void listRemoveMiddle(ListType & list, string target)
{
    ListType p = list;
    while(p->next->key != target)
    {
        p = p->next; 
    }
    ListType q = p->next;
    p->next= p->next->next;
    delete q;
}

//Remove last element of list. 
void listRemoveLast(ListType & list, string target)
{
    ListType p = list;
    while(p->next->next != NULL )
    {
        p = p->next; 
    }
    ListType q = p->next;
    delete q;
    p->next = NULL;
}

//Insert an element to the list.
//return bool if the element was inserted.
bool listInsert(ListType & list, string target, int value)
{   
    if(listLookup(list,target) == NULL)
    {
        list = new Node(target, value, list);
        return true;
    }   
    return false;
}

//Lookup for an element in list.
//return NULL if element is not present ow return a pointer to value. 
int * listLookup (ListType & list, string target)
{
    ListType p = list;
    while(p != NULL)
    {
        if(p-> key == target)
        {
            return &p->value;
        }
        p= p->next; 
    }
    return NULL;    
}
